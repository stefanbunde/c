#include <stdio.h>

#include "arrays.h"
#include "enums.h"
#include "strings.h"
#include "structs.h"
#include "unions.h"


void print_stars() {
    printf("********************************************\n");
}


void print_headline(char *headline) {
    print_stars();
    printf("%25s\n", headline);
    print_stars();
}


int main() {
    print_headline("Arrays");
    arrays();

    print_headline("Strings");
    strings();

    print_headline("Structs");
    structs();

    print_headline("Enums");
    enums();

    print_headline("Unions");
    unions();

    return 0;
}
