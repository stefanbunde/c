#include <stdio.h>


void define_and_initialize_strings();
void fill_string_from_user_input_and_print_to_screen();
void copy_string();
void copy_string_short_form();


void strings() {
    define_and_initialize_strings();
    fill_string_from_user_input_and_print_to_screen();
    copy_string();
    copy_string_short_form();
}


void define_and_initialize_strings() {
    // string is a special case of an array
    char city[50];

    // like an array it can be initialized with default values
    char first_name[] = {'W', 'i', 'n', 's', 't', 'o', 'n'};

    // to ease initialization take quotes
    char name[] = "Winston Wolf";
}


void fill_string_from_user_input_and_print_to_screen() {
    char city[100];

    printf("Enter your current city, please: ");
    scanf("%99s", city);
    printf("You are living in %s.\n\n", city);
}


void copy_string() {
    int i = 0;
    char name[] = "Winston Wolf";
    char username[50];

    printf("Copy name \"%s\" to username\n", name);
    while (name[i]) {
        username[i] = name[i];
        i++;
    }
    username[i] = '\0';
    printf("Content of username: %s.\n\n", username);
}


void copy_string_short_form() {
    int i = 0;
    char name[] = "Winston Wolf";
    char username[50];

    printf("Short form to copy name \"%s\" to username\n", name);
    while (username[i] = name[i])
        i++;
    printf("Content of username: %s.\n\n", username);
}
