#include <stdio.h>


void define_and_initialize_arrays();
void print_array_size_in_bytes();
void fill_array_from_user_input_and_print_to_screen();
void define_a_multidimensional_array();


void arrays() {
    define_and_initialize_arrays();
    print_array_size_in_bytes();
    fill_array_from_user_input_and_print_to_screen();
    define_a_multidimensional_array();
}


void define_and_initialize_arrays() {
    // define an integer array. It has 10 elements with unknown values
    int values[10];

    // define an integer array. It has 5 elements with default values
    int items[5] = {1, 2, 3, 4, 5};

    // define an integer array with 6 default values. That leads to an array size of 6 elements
    int numbers[] = {1, 2, 3, 4, 5, 6};
}


void print_array_size_in_bytes() {
    int array_size = 10;
    int numbers[array_size];

    printf("Number of elements: %i\n", array_size);
    printf("Array size in bytes: %i\n", (int) (array_size * sizeof(int)));
    printf("Array size in bytes: %i\n", (int) (array_size * sizeof(numbers[0])));
    printf("\n");
}


void fill_array_from_user_input_and_print_to_screen() {
    int i, array_size = 3;
    int numbers[array_size];

    printf("Please enter %i numbers.\n", array_size);
    for (i=0; i<array_size; i++)
        scanf("%d", &numbers[i]);

    printf("Your input was: ");
    for (i=0; i<array_size; i++)
        printf("%i  ", numbers[i]);
    printf("\n\n");
}


void define_a_multidimensional_array() {
    int i, j;
    int matrix[3][3] = { {1, 2, 3}, {4, 5, 6}, {7, 8, 9} };

    printf("This is a matrix\n");
    for (i=0; i<3; i++) {
        for (j=0; j<3; j++)
            printf("%3i ", matrix[i][j]);
        printf("\n");
    }
    printf("\n");
}
