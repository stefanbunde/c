#include <stdio.h>
#include <string.h>


void define_a_struct();
void print_struct_size_in_bytes();
void fill_struct_and_print_to_screen();


void structs() {
    define_a_struct();
    print_struct_size_in_bytes();
    fill_struct_and_print_to_screen();
}


void define_a_struct() {
    // define a struct with the name Book
    struct Book {
        char title[50];
        char author[50];
        float price;
    };

    // define a struct and define an array of this type
    struct Location {
        short int x;
        short int y;
    } locations[10];

    // define a struct where the elements only use single bits
    struct Bitfield {
        unsigned char flag_a: 1;  // 1 bit
        unsigned char flag_b: 1;  // 1 bit
        unsigned char mode: 2;    // 2 bits
        unsigned char status: 4;  // 4 bits
    };

    // define variables of struct type Book
    struct Book single_book;
    struct Book another_array_of_books[5];
}


void print_struct_size_in_bytes() {
    struct Book {
        char title[50];
        char author[50];
        float price;
    };

    struct Bitfield {
        unsigned char flag: 1;
        unsigned char mode: 2;
        unsigned char status: 5;
    };


    printf("struct Book {\n" \
           "    char title[50];\n" \
           "    char author[50];\n" \
           "    float price;\n" \
           "};\n");
    printf("Struct size in bytes: %i\n\n", (int) sizeof(struct Book));


    printf("struct Bitfield {\n"
           "    unsigned char flag: 1;\n"
           "    unsigned char mode: 2;\n"
           "    unsigned char status: 5;\n"
           "};\n");
    printf("Struct size in bytes: %i\n\n", (int) sizeof(struct Bitfield));
}


void fill_struct_and_print_to_screen() {
    struct Book {
        char title[50];
        char author[50];
        float price;
    } book;

    strcpy(book.title, "The Alchemist");
    strcpy(book.author, "Paulo Coelho");
    book.price = 12.99;

    printf("The book '%s' written by '%s' costs %.2f EUR.\n\n", book.title, book.author, book.price);
}
