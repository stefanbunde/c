#include <stdio.h>


void define_enums();
void print_enum_size_in_bytes();


void enums() {
    define_enums;
    print_enum_size_in_bytes();
}


void define_an_enum_and_create_variables_of_this_type() {
    // define enum with 7 values and define a variable of this type
    enum Weekday {
        Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
    } tomorrow;

    // define variable of enum type
    enum Weekday today = Tuesday;

    // define enum with initial values
    enum Colours {
        White = 0,
        Blue = 25,
        Green = 50,
        Red = 75,
        Yellow = 100
    };
}


void print_enum_size_in_bytes() {
    enum Weekday {Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, NumberOfWeekdayItems};

    printf("Number of elements: %i\n", NumberOfWeekdayItems);
    printf("Enum size in bytes: %i\n\n", (int) sizeof(enum Weekday));
}
