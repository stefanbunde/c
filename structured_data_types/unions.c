#include <stdio.h>


void define_unions();
void print_union_size_in_bytes();
void fill_union_from_user_input_and_print_to_srceen();


void unions() {
    define_unions();
    print_union_size_in_bytes();
    fill_union_from_user_input_and_print_to_srceen();
}


void define_unions() {
    // define an union and define a variable of this type
    union Number {
        int value;
        unsigned char bytes[sizeof(int)];
    } number;

    // define an array of union type
    union Number numbers[10];
}


void print_union_size_in_bytes() {
    union Number {
        int value;
        unsigned char bytes[sizeof(int)];
    };

    printf("union Number {\n" \
           "    int value;\n" \
           "    unsigned char bytes[sizeof(int)];\n" \
           "};\n");
    printf("Union size in bytes: %i\n\n", (int) sizeof(union Number));
}


void fill_union_from_user_input_and_print_to_srceen() {
    int i;
    union Number {
        int value;
        unsigned char bytes[sizeof(int)];
    } number;

    do {
        printf("Please enter a number (quit with 0): ");
        scanf("%i", &number.value);
        printf("display the number %i in byte-form: \n", number.value);
        for (i=sizeof(int) - 1; i >= 0; i--)
            printf("byte number: %i = %i\n", i, number.bytes[i]);
        printf("\n");
    } while (number.value);
}
